package nautilus.game.core.arena;

public abstract interface IArenaReloadedListener
{
  public abstract void ArenaReloaded(Arena paramArena);
}
